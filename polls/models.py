from django.db import models
import datetime
from django.utils import timezone

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class TeamInfo(models.Model):
    #id = models.AutoField(primary_key=True)
    TeamName = models.CharField(max_length = 200,default = "")
    TeamCountry = models.CharField(max_length = 200)
    TeamLeague = models.CharField(max_length = 200)

    def __str__(self):
        return self.TeamName


class Statistics(models.Model):
    #zamienic tutaj foregin key o nazwie fixtures
    fixtures_id = models.IntegerField(primary_key=True)
    assist_home = models.IntegerField()
    blocked_shoots_home = models.IntegerField()
    corners_home = models.IntegerField()
    counter_attacks_home = models.IntegerField()
    cross_attacks_home = models.IntegerField()
    fouls_home = models.IntegerField()
    free_kicks_home = models.IntegerField()
    goals_home = models.IntegerField()
    goal_attempts_home = models.IntegerField()
    offsides_home = models.IntegerField()
    ball_possesion_home = models.IntegerField()
    red_cards_home = models.IntegerField()
    goalkeepers_save_home = models.IntegerField()
    shoots_off_goal_home = models.IntegerField()
    shoots_on_home = models.IntegerField()
    substitusions_home = models.IntegerField()
    throw_ins_home = models.IntegerField()
    medical_treatments_home = models.IntegerField()
    yellow_cards_home = models.IntegerField()
    assist_away = models.IntegerField()
    blocked_shoots_away = models.IntegerField()
    corners_away = models.IntegerField()
    counter_attacks_away = models.IntegerField()
    cross_attacks_away = models.IntegerField()
    fouls_away = models.IntegerField()
    free_kicks_away = models.IntegerField()
    goals_away = models.IntegerField()
    goal_attempts_away = models.IntegerField()
    offsides_away = models.IntegerField()
    ball_possesion_away = models.IntegerField()
    red_cards_away = models.IntegerField()
    goalkeepers_save_away = models.IntegerField()
    shoots_off_goal_away = models.IntegerField()
    shoots_on_away = models.IntegerField()
    substitusions_away = models.IntegerField()
    throw_ins_away = models.IntegerField()
    medical_treatments_away = models.IntegerField()
    yellow_cards_away = models.IntegerField()

    def __str__(self):
        return self.fixtures_id

class Fixtures(models.Model):
    #tutaj primary key
    fixtures_id = models.ForeignKey(Statistics,on_delete = models.CASCADE,to_field = 'fixtures_id')
    home_team_id = models.IntegerField()
    away_team_id = models.IntegerField()



