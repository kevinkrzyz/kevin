from django.urls import path,include
from django.contrib import admin


from . import views

app_name = 'polls'
urlpatterns = [
    path('',views.IndexView.as_view(), name = 'index'),
    path('<int:pk>/', views.DetailView.as_view(), name = 'detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name = 'results'),
    path('<int:question_id>/vote/',views.vote, name = 'vote'),
    path('<int:pk>/team/', views.StatsView.as_view(), name='team'),
    path('api/chart/', views.chart, name='api-chart'),
    path('<int:pk>/api/data/', views.GetData.as_view(), name='api-data'),
    path('<int:pk>/charts_select/', views.Charts_select.as_view(), name='charts_select'),
    #path('<int:TeamNumber>/team/', views.StatsView, name='team'),
]