from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.template import loader
from .models import Question,Choice,TeamInfo
from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.urls import reverse
from django.views import generic

from polls.templates.polls.static.fusioncharts import FusionCharts


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

class StatsView(generic.DetailView):
    model = TeamInfo
    template_name = 'polls/team.html'

class Charts_select(generic.DetailView):
    model = TeamInfo
    template_name = 'polls/charts_select.html'



def vote(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError,Choice.DoesNotExist):
        return render(request,'polls/detail.html',{'question':question,
                                                   'error_message':"You didn't select a choice"})
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results',args=(question.id,)))

class GetData(generic.DetailView):
#def get_data(request):
    '''data = {
        "sales": 1000,
        'customers': 10,
    }'''
    data = 1000
    template_name = 'polls/charts.html'
    #return JsonResponse(data)



def chart(request):
    # Create an object for the column2d chart using the FusionCharts class constructor
  column2d = FusionCharts("column2d", "ex1" , "684", "476", "chart-1", "json",
        # The data is passed as a string in the `dataSource` as parameter.
    """{  
        "chart": {
        "caption": "Most Popular Superhero on Youtube",
        "baseFont": "Lato",
        "captionfontsize":"18",
        "subcaptionfontbold":"0",
        "subcaptionfontsize":"14",
        "subcaption": "Jan 2008 - September 2017",
        "yaxisname": "Total time spent watching",
        "captionpadding": "20",
        "bgalpha": "0",
        "canvasbgalpha": "0",
        "showvalues": "0",
        "showborder": "0",
        "canvasborderalpha": "0",
        "showalternatehgridcolor": "0",
        "plotgradientcolor": "",
        "showplotborder": "0",
        "adjustDiv":"0",
        "yaxisnamefontsize":"14",
        "yAxisNameFontBold":"0",
        "yAxisValuesPadding":"18",
        "divlinealpha": "10",
        "xaxislinealpha":"20",
        "LabelPadding": "50",
        "showlabels": "0",
        "numdivlines":"4",
        "showxaxisline":"1",
        "plotspacepercent":"40",
        "yAxisValueDecimals":"0",
        "formatnumberscale": "1",
        "numberscalevalue": "24,31,12",
        "numberscaleunit": " days, months, years",
        "palettecolors": "#3F365A",
        "plotToolText": "<div>Superhero : <b>$label</b><br/>Time Spend : <b>$value Hours</b></div>",
        "defaultnumberscale": " years",
        "plotFillAlpha": "90"
      },
      "annotations": {
        "autoScale": "0",
        "scaleImages": "0",
        "origW": "400",
        "origH": "300",
        "groups": [{
          "id": "user-images",
          "items": [{
            "id": "Batman-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/batman.png",
            "x": "$dataset.0.set.0.CenterX - 18",
            "y": "$dataset.0.set.0.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "Wolverine-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/wolverine.png",
            "x": "$dataset.0.set.1.CenterX - 18",
            "y": "$dataset.0.set.1.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "IronMan-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/ironman.png",
            "x": "$dataset.0.set.2.CenterX - 18",
            "y": "$dataset.0.set.2.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "Deadpool-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/deadpool.png",
            "x": "$dataset.0.set.3.CenterX - 18",
            "y": "$dataset.0.set.3.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "SpiderMan-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/spiderman.png",
            "x": "$dataset.0.set.4.CenterX - 18",
            "y": "$dataset.0.set.4.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "Thor-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/thor.png",
            "x": "$dataset.0.set.5.CenterX - 18",
            "y": "$dataset.0.set.5.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "SuperMan-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/superman.png",
            "x": "$dataset.0.set.6.CenterX - 18",
            "y": "$dataset.0.set.6.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }, {
            "id": "CaptainAmerica-icon",
            "type": "image",
            "url": "http://csm.fusioncharts.com/files/assets/img/captain-america.png",
            "x": "$dataset.0.set.7.CenterX - 18",
            "y": "$dataset.0.set.7.EndY + 10",
            "xScale": "75",
            "yScale": "75"
          }]
        }]
      },
      "data": [{
        "label": "Batman",
        "value": "85000"
      }, {
        "label": "Wolverine",
        "value": "82000"
      }, {
        "label": "Iron Man",
        "value": "58000"
      }, {
        "label": "Deadpool",
        "value": "42000"
      }, {
        "label": "Spider Man",
        "value": "36000"
      }, {
        "label": "Thor",
        "value": "21000"
      }, {
        "label": "Super Man",
        "value": "18000"
      }, {
        "label": "Captain A",
        "value": "6000"
      }]
      }""")

    # returning complete JavaScript and HTML code, which is used to generate chart in the browsers.
  return  render(request, 'index_chart.html', {'output' : column2d.render()})