from django.test import TestCase
from django.urls import resolve
from polls.views import DetailView
from django.urls import reverse
from django.http import HttpRequest
from polls.models import TeamInfo

# Create your tests here.
class PollsTeamTest(TestCase):

    def setUp(self):
        self.q = TeamInfo(id = 20,TeamName = 'Tworkow',TeamCountry = 'Poland', TeamLeague = 'A Klasa')
        self.q.save()

    '''def compare(self):
        self.a = TeamInfo.objects.get(pk = 6)
        self.a.save()'''

    def test_team_id_request(self):
        TeamName_test = self.q.TeamName
        TeamCountry_test = self.q.TeamCountry
        TeamLeague_test = self.q.TeamLeague
        a = TeamInfo.objects.get(pk=5)
        Name = a.TeamName
        Country = a.TeamCountry
        League = a.TeamLeague

        self.assertEqual(Name,TeamName_test)
        self.assertEqual(Country, TeamCountry_test)
        self.assertEqual(League, TeamLeague_test)