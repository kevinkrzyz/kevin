from selenium import webdriver
import unittest

class NewVisitorTest:

    def setup(self):
        self.browser = webdriver.Chrome(executable_path='C:/Users/krzyzok/Documents/chr/chromedriver.exe')

    def turnDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrive_it_later(self):
        self.browser.get('http://localhost:8000/polls')
        self.assertIn('To-do',self.browser.title)
        self.fail('Finish the test')

if __name__ == '__main__':
    unittest.main(warnings='ignore')

